package response.influencer

data class Credits(
    val single_credits: Int = 0,
    val monthly_credits: Int = 0,
    val used_credits: Int = 0,
    val total_available_credits: Int = 0,
    val monthly_credits_days_remaining: Int = 0,
    val credits_from_influencer_uid: String = ""
)