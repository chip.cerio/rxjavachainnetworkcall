package response.influencer

import response.Response

data class InfluencerResponse(
    val data: InfluencerMeta = InfluencerMeta()
) : Response()