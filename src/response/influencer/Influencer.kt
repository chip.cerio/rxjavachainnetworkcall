package response.influencer

data class Influencer(
    val first_name: String = "",
    val last_name: String = "",
    val full_name: String = "",
    val show_waitlist_to_ask_question: Boolean = false
)