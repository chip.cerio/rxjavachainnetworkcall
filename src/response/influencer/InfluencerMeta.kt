package response.influencer

data class InfluencerMeta(
    val influencer: Influencer = Influencer(),
    val question_credits: Credits = Credits()
)