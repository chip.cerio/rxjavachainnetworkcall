package response

abstract class Response(
    val success: Boolean = false,
    val message: String = "",
    val http_status: Int = 0
)