package response.pricing

data class PricingMeta(
    val package_settings: PricingSettings = PricingSettings(),
    val package_pricing: PackagePricing = PackagePricing()
)