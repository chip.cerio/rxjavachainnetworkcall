package response.pricing

data class Pricing(
    val package_id: Int = 0,
    val quantity: Int? = 0,
    val price: Float = 0f,
    val stripe_plan_id: String? = "",
    val is_active: Boolean? = false
)