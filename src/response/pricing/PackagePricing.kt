package response.pricing

data class PackagePricing(
    val monthly: List<Pricing> = emptyList(),
    val single: List<Pricing> = emptyList(),
    val vip: Pricing = Pricing()
)