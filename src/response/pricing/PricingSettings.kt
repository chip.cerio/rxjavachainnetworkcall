package response.pricing

data class PricingSettings(
    val is_single_pack_enabled: Boolean = false,
    val is_monthly_pack_enabled: Boolean = false,
    val is_vip_pack_enabled: Boolean = false
)