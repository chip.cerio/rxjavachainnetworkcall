package response.pricing

import response.Response

data class PricingResponse(
    val data: PricingMeta = PricingMeta()
) : Response()