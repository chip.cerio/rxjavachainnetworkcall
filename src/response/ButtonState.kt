package response

data class ButtonState(
    var isOnWaitList: Boolean = false,
    var totoalCredits: Int = 0,
    var price: Float = 0f
)