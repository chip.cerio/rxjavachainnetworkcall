import okhttp3.OkHttpClient
import okhttp3.Request

object Http {

    const val BASE_URL = "http://ec2-18-221-250-200.us-east-2.compute.amazonaws.com/api/v1"

    val client = OkHttpClient()

    fun get() {

    }

    fun post(endpoint: String) {
        val request = Request.Builder()
            .url(BASE_URL)
            .build()

        client.newCall(request).execute()
    }
}