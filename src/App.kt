import com.google.gson.Gson
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.functions.BiFunction
import okhttp3.*
import response.ButtonState
import response.influencer.InfluencerResponse
import response.pricing.PricingResponse
import java.io.IOException

fun main() {
    val influencerUid = "DNXA130"

    // use zip operator
    // https://stackoverflow.com/a/42082076
    observeButtonState(influencerUid)
        .subscribe(
            { println("ButtonState: $it") },
            { System.err.println(it.message) })
}

private fun observeButtonState(id: String): Observable<ButtonState> {
    return Observable.zip(
        getInfluencerProfile(id),
        getPackagePricing(id),
        BiFunction { emission1, emission2 ->
            ButtonState(
                isOnWaitList = emission1.data.influencer.show_waitlist_to_ask_question,
                totoalCredits = emission1.data.question_credits.total_available_credits,
                price = emission2.data.package_pricing.monthly[0].price
            )
        }
    )
}

private fun getInfluencerProfile(id: String): Observable<InfluencerResponse> {
    return Observable.create { emitter ->
        val request = Request.Builder()
            .url("${Http.BASE_URL}/influencer/$id")
            .method("GET", null)
            .addHeader("Authorization", "Bearer ${Creds.ALICE_TOKEN}")
            .build()

        Http.client.newCall(request).execute().use { response ->
            if (!response.isSuccessful) {
                val err = IOException("Error in getting influencer\'s profile")
                emitter.onError(err)
            }

            val responseString = response.body!!.string()
            val gson = Gson()
            val influencerRes = gson.fromJson(responseString, InfluencerResponse::class.java)

            emitter.onNext(influencerRes)
            emitter.onComplete()
        }
    }
}

private fun getPackagePricing(id: String): Observable<PricingResponse> {
    return Observable.create { emitter ->
        val request = Request.Builder()
            .url("${Http.BASE_URL}/influencer/$id/package/pricing")
            .method("GET", null)
            .addHeader("Authorization", "Bearer ${Creds.ALICE_TOKEN}")
            .build()

        Http.client.newCall(request).execute().use { response ->
            if (!response.isSuccessful) {
                val err = IOException("Error in getting influencer\'s pricing")
                emitter.onError(err)
            }

            val responseString = response.body!!.string()
            val gson = Gson()
            val pricingRes = gson.fromJson(responseString, PricingResponse::class.java)

            emitter.onNext(pricingRes)
            emitter.onComplete()
        }
    }
}